<?php

namespace DomainActions\Tests;

use DomainActions\DomainAction;
use Illuminate\Log\LogManager;
use Exception;
use Mockery as m;
use Psr\Log\LoggerInterface;

/**
 * @covers \DomainActions\DomainAction
 * @covers \DomainActions\DomainActionResult
 */
class DomainActionLoggingTest extends TestCase
{
    use MockLoggerTrait;

    public function testMessagesDefaultLogging(): void
    {
        $this->mockLogger(7);

        $action = new class extends DomainAction {
            protected function setParameters(array $parameters): void {
                $this->addMessage('message_debug_only', true);
            }

            protected function process(): array {
                $this->addMessage('message');
                $this->addMessage('message_debug_only', true);

                throw new Exception();
            }
        };;

        $result = $action->set()->run();

        $this->assertCount(3, $result->messages());
    }

    public function testMessagesActionsLogging(): void
    {
        config([
            'logging' => [
                'channels' => [
                    'actions' => [
                        'driver' => 'stack',
                    ]
                ]
            ]
        ]);

        $this->mockLogger(7, 'actions');

        $action = new class extends DomainAction {
            protected function setParameters(array $parameters): void {
                $this->addMessage('message_debug_only', true);
            }

            protected function process(): array {
                $this->addMessage('message');
                $this->addMessage('message_debug_only', true);

                throw new Exception();
            }
        };;

        $result = $action->set()->run();

        $this->assertCount(3, $result->messages());
    }

    public function testLoggerDisable(): void
    {
        $logger = m::mock(LoggerInterface::class);
        $logger->shouldReceive('info')
            ->withAnyArgs()
            ->never();

        $logManager = m::mock(LogManager::class);
        $logManager->shouldReceive('channel')
            ->never()
            ->with('stack');

        $this->instance('log', $logManager);

        $action = new class extends DomainAction {
            protected bool $has_logging = false;

            protected function setParameters(array $parameters): void {
                $this->addMessage('message_debug_only', true);
            }

            protected function process(): array {
                $this->addMessage('message');
                $this->addMessage('message_debug_only', true);

                throw new Exception();
            }
        };;

        $result = $action->set()->run();

        $this->assertCount(3, $result->messages());
    }
}