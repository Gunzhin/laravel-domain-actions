<?php

namespace DomainActions\Tests;

use DomainActions\DomainAction;
use DomainActions\Exceptions\InvalidActionParameters;
use Illuminate\Log\LogManager;
use Illuminate\Support\Collection;
use Exception;
use Mockery as m;
use Psr\Log\LoggerInterface;

/**
 * @covers \DomainActions\DomainAction
 * @covers \DomainActions\DomainActionResult
 */
class DomainActionTest extends TestCase
{
    use MockLoggerTrait;

    public function testConstructor(): void
    {
        $action = new class extends DomainAction {
            protected function setParameters(array $parameters): void {}

            protected function process(): array {
                return [];
            }
        };

        $this->assertInstanceOf(DomainAction::class, $action);
    }

    public function testSetFailed(): void
    {
        $this->mockLogger(0);

        $this->expectException(InvalidActionParameters::class);

        $action = new class extends DomainAction {
            public string $value;

            protected function setParameters(array $parameters): void {
                $this->value = $parameters[0];
            }

            protected function process(): array {
                return [];
            }

            public function getMessages(): Collection {
                return $this->messages;
            }
        };

        $action->set();
    }

    public function testSetSuccess(): void
    {
        $this->mockLogger(1);

        $action = new class extends DomainAction {
            public string $value;

            protected function setParameters(array $parameters): void {
                $this->value = $parameters[0];
            }

            protected function process(): array {
                return [];
            }

            public function getMessages(): Collection {
                return $this->messages;
            }
        };

        $action->set(
            $value = $this->faker->word
        );

        $this->assertEquals($value, $action->value);
        $this->assertCount(1, $messages = $action->getMessages());
        $this->assertStringContainsString($value, $messages->first());
    }

    public function testRunSuccess(): void
    {
        $this->mockLogger(2);

        $action = new class extends DomainAction {
            protected function setParameters(array $parameters): void {}

            protected function process(): array {
                $this->addMessage('message');

                return [
                    'key' => 'value',
                ];
            }
        };

        $result = $action->run();

        $this->assertTrue($result->isSuccess());
        $this->assertCount(1, $result->messages());
        $this->assertEquals(['key' => 'value'], $result->payload()->toArray());
        $this->assertNull($result->exception());
    }

    public function testRunException(): void
    {
        $this->mockLogger(4);

        $action = new class extends DomainAction {
            protected function setParameters(array $parameters): void {}

            protected function process(): array {
                $this->addMessage('message');

                throw new Exception();
            }
        };

        $result = $action->run();

        $this->assertFalse($result->isSuccess());
        $this->assertCount(2, $result->messages());
        $this->assertEmpty($result->payload()->toArray());
        $this->assertInstanceOf(Exception::class, $result->exception());
    }

    /**
     * @param int $call_times
     * @param string|null $channel
     *
     * @return void
     */
    protected function mockLogger(int $call_times, string $channel = null): void
    {
        $channel ??= 'stack';

        $logger = m::mock(LoggerInterface::class);
        $logger->shouldReceive('info')
            ->withAnyArgs()
            ->times($call_times)
            ->andReturnNull();

        $logManager = m::mock(LogManager::class);
        $logManager->shouldReceive('channel')
            ->once()
            ->with($channel)
            ->andReturn($logger);

        $this->instance('log', $logManager);
    }
}