<?php

namespace DomainActions\Tests;

use DomainActions\DomainActionResultHttpArray;
use Illuminate\Support\Collection;
use Exception;

/**
 * @covers \DomainActions\DomainActionResultHttpArray
 * @covers \DomainActions\DomainActionResult
 */
class DomainActionResultHttpArrayTest extends TestCase
{
    public function testPresentDefaultSuccess(): void
    {
        $result = $this->makeResult(
            'SUCCESS',
            new Collection([
                $this->faker->sentence,
                $message = $this->faker->sentence,
            ]),
            new Collection($payload = [
                $this->faker->word => $this->faker->sentence,
                $this->faker->word => $this->faker->sentence,
            ]),
        );

        $view = new DomainActionResultHttpArray();

        $expected = array_merge(
            [
                'status' => 'SUCCESS',
                'message' => $message,
            ],
            $payload
        );

        $presented = $view->present($result);

        $this->assertEquals($expected, $presented);
    }

    public function testPresentDefaultException(): void
    {
        $result = $this->makeResult(
            'EXCEPTION_ERROR',
            new Collection([
                $this->faker->sentence,
                $message = $this->faker->sentence,
            ]),
            new Collection(),
            new Exception()
        );

        $view = new DomainActionResultHttpArray();

        $expected = [
            'status' => 'EXCEPTION_ERROR',
            'message' => $message,
        ];

        $presented = $view->present($result);

        $this->assertEquals($expected, $presented);
    }

    public function testPresentDebugException(): void
    {
        config([
            'app.debug' => true
        ]);

        $result = $this->makeResult(
            'EXCEPTION_ERROR',
            new Collection([
                $this->faker->sentence,
                $message = $this->faker->sentence,
            ]),
            new Collection(),
            $exception = new Exception()
        );

        $view = new DomainActionResultHttpArray();

        $expected = [
            'status' => 'EXCEPTION_ERROR',
            'message' => $message,
            'exception' => (string) $exception
        ];

        $presented = $view->present($result);

        $this->assertEquals($expected, $presented);
    }

    public function testPresentClosurePartialReplacedSuccess(): void
    {
        $result = $this->makeResult(
            'SUCCESS',
            new Collection([
                $this->faker->sentence,
                $message = $this->faker->sentence,
            ]),
            new Collection($payload = [
                'key_1' => $this->faker->sentence,
                'key_2' => $this->faker->sentence,
            ]),
        );

        $view = new DomainActionResultHttpArray();

        $additional = [
            'key_2' => $this->faker->sentence,
            'key_3' => $this->faker->sentence,
        ];

        $presented = $view->present($result, fn (array $data) => array_merge($data, $additional));

        $expected = [
            'status' => 'SUCCESS',
            'message' => $message,
            'key_1' => $payload['key_1'],
            'key_2' => $additional['key_2'],
            'key_3' => $additional['key_3'],
        ];

        $this->assertEquals($expected, $presented);
    }
}