<?php

namespace DomainActions\Tests;

use Illuminate\Log\LogManager;
use Psr\Log\LoggerInterface;
use Mockery as m;

/**
 * @mixin TestCase
 */
trait MockLoggerTrait
{
    /**
     * @param int $call_times
     * @param string|null $channel
     *
     * @return void
     */
    protected function mockLogger(int $call_times, string $channel = null): void
    {
        $channel ??= 'stack';

        $logger = m::mock(LoggerInterface::class);
        $logger->shouldReceive('info')
            ->withAnyArgs()
            ->times($call_times)
            ->andReturnNull();

        $logManager = m::mock(LogManager::class);
        $logManager->shouldReceive('channel')
            ->once()
            ->with($channel)
            ->andReturn($logger);

        $this->instance('log', $logManager);
    }
}