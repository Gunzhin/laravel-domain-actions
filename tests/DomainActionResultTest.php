<?php

namespace DomainActions\Tests;

use DomainActions\DomainActionResult;
use DomainActions\Exceptions\InvalidResultParameters;
use Illuminate\Support\Collection;
use Exception;

/**
 * @covers \DomainActions\DomainActionResult
 */
class DomainActionResultTest extends TestCase
{
    public function testConstructorIncorrectStatus(): void
    {
        $this->expectException(InvalidResultParameters::class);

        $this->makeResult(
            $this->faker->word,
            new Collection(),
            new Collection(),
            null
        );
    }

    public function testConstructorSuccessStatusWithoutMessages(): void
    {
        $this->expectException(InvalidResultParameters::class);

        $this->makeResult(
            'SUCCESS',
            new Collection(),
            new Collection(),
            null
        );
    }

    public function testConstructorSuccessStatusWithException(): void
    {
        $this->expectException(InvalidResultParameters::class);

        $this->makeResult(
            'SUCCESS',
            new Collection([
                $this->faker->sentence
            ]),
            new Collection(),
            new Exception()
        );
    }

    public function testConstructorSuccessStatus(): void
    {
        $result = $this->makeResult(
            'SUCCESS',
            new Collection([
                $this->faker->sentence
            ]),
            new Collection(),
            null
        );

        $this->assertInstanceOf(DomainActionResult::class, $result);
    }

    public function testConstructorExceptionStatusWithoutException(): void
    {
        $this->expectException(InvalidResultParameters::class);

        $this->makeResult(
            'EXCEPTION_ERROR',
            new Collection([
                $this->faker->sentence
            ]),
            new Collection(),
            null
        );
    }


    public function testConstructorExceptionStatusWithPayload(): void
    {
        $this->expectException(InvalidResultParameters::class);

        $this->makeResult(
            'EXCEPTION_ERROR',
            new Collection([
                $this->faker->sentence
            ]),
            new Collection([
                $this->faker->word => $this->faker->sentence
            ]),
            new Exception()
        );
    }

    public function testConstructorExceptionStatus(): void
    {
        $result = $this->makeResult(
            'EXCEPTION_ERROR',
            new Collection([
                $this->faker->sentence
            ]),
            new Collection(),
            new Exception()
        );

        $this->assertInstanceOf(DomainActionResult::class, $result);
    }

    public function testConstructorRandomErrorStatus(): void
    {
        $result = $this->makeResult(
            $this->getRandomErrorStatus()
        );

        $this->assertInstanceOf(DomainActionResult::class, $result);
    }

    public function testStatus(): void
    {
        $status = $this->getRandomErrorStatus();

        $result = $this->makeResult($status);

        $this->assertEquals($status, $result->status());
    }

    public function testIsSuccessTrue(): void
    {
        $status = 'SUCCESS';

        $result = $this->makeResult($status);

        $this->assertTrue($result->isSuccess());
    }

    public function testIsSuccessFalse(): void
    {
        $status = $this->getRandomErrorStatus();

        $result = $this->makeResult($status);

        $this->assertFalse($result->isSuccess());
    }

    public function testMessages(): void
    {
        $messages = new Collection([
            $this->faker->sentence,
            $this->faker->sentence,
            $this->faker->sentence,
        ]);

        $result = $this->makeResult(null, $messages);

        $this->assertEquals($messages->toArray(), $result->messages()->toArray());
    }

    public function testPayload(): void
    {
        $payload = new Collection([
            $this->faker->word => $this->faker->word,
            $this->faker->word => $this->faker->sentence,
            $this->faker->word => new Collection([
                $this->faker->word => $this->faker->word,
                $this->faker->word => $this->faker->sentence,
            ]),
        ]);

        $result = $this->makeResult(
            'SUCCESS',
            new Collection([
                $this->faker->sentence
            ]),
            $payload
        );

        $this->assertEquals($payload->toArray(), $result->payload()->toArray());
    }

    public function testException(): void
    {
        $exception = new Exception($this->faker->sentence);

        $result = $this->makeResult(
            'EXCEPTION_ERROR',
            new Collection([
                $this->faker->sentence
            ]),
            new Collection(),
            $exception
        );

        $this->assertEquals((string) $exception, (string) $result->exception());
    }

    public function testSuccessToArray(): void
    {
        $result = $this->makeResult(
            $status = 'SUCCESS',
            $messages = new Collection([
                $this->faker->sentence
            ]),
            $payload = new Collection([
                $this->faker->word => $this->faker->sentence
            ]),
        );

        $this->assertEquals(
            [
                'status' => $status,
                'messages' => $messages->toArray(),
                'payload' => $payload->toArray(),
                'exception' => null,
            ],
            $result->toArray()
        );
    }

    public function testExceptionErrorToArray(): void
    {
        $result = $this->makeResult(
            $status = 'EXCEPTION_ERROR',
            $messages = new Collection([
                $this->faker->sentence
            ]),
            $payload = new Collection(),
            $exception = new Exception()
        );

        $this->assertEquals(
            [
                'status' => $status,
                'messages' => $messages->toArray(),
                'payload' => $payload->toArray(),
                'exception' => (string) $exception,
            ],
            $result->toArray()
        );
    }
}