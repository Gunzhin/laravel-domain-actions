<?php

namespace DomainActions\Tests;

use DomainActions\DomainActionResult;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Collection;
use Orchestra\Testbench\TestCase as BaseTestCase;
use Throwable;

abstract class TestCase extends BaseTestCase
{
    use WithFaker;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpFaker();
    }


    /**
     * @param string|null $status
     * @param Collection|null $messages
     * @param Collection|null $payload
     * @param Throwable|null $exception
     *
     * @return DomainActionResult
     *
     * @throws
     */
    protected function makeResult(
        string $status = null,
        Collection $messages = null,
        Collection $payload = null,
        Throwable $exception = null
    ): DomainActionResult
    {
        if ($status === null) {
            $status = $this->getRandomErrorStatus();
        }

        if ($messages === null) {
            $messages = new Collection([
                $this->faker->sentence
            ]);
        }

        if ($payload === null) {
            $payload = ($status === 'SUCCESS')
                ? new Collection([
                    $this->faker->word => $this->faker->sentence,
                ])
                : new Collection();
        }

        return new DomainActionResult($status, $messages, $payload, $exception);
    }

    /**
     * @return string
     */
    protected function getRandomErrorStatus(): string
    {
        $statuses = $this->getAvailableResultStatuses();

        array_shift($statuses);
        array_pop($statuses);

        return $this->faker->randomElement($statuses);
    }

    /**
     * @return string[]
     */
    protected function getAvailableResultStatuses(): array
    {
        return [
            'SUCCESS',
            'FAILED',
            'VALIDATION_ERROR',
            'FORBIDDEN_ERROR',
            'NOT_FOUND_ERROR',
            'EXCEPTION_ERROR',
        ];
    }
}