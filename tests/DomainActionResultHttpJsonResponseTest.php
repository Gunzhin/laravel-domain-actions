<?php

namespace DomainActions\Tests;

use DomainActions\Contracts\DomainActionResultContract;
use DomainActions\DomainActionResultHttpArray;
use DomainActions\DomainActionResultHttpJsonResponse;
use Illuminate\Http\JsonResponse;
use Mockery as m;

/**
 * @covers \DomainActions\DomainActionResultHttpJsonResponse
 * @covers \DomainActions\DomainActionResult
 */
class DomainActionResultHttpJsonResponseTest extends TestCase
{
    public function testPresentDefaultSuccess(): void
    {
        $result = m::mock(DomainActionResultContract::class);
        $result->shouldReceive('status')
            ->once()
            ->andReturn($this->faker->word);

        $closure = fn () => null;

        $baseView = m::mock(DomainActionResultHttpArray::class);
        $baseView->shouldReceive('present')
            ->once()
            ->with($result, $closure)
            ->andReturn($data = [
                $this->faker->word => $this->faker->sentence,
            ]);

        $view = new DomainActionResultHttpJsonResponse($baseView);

        $response = $view->present($result, $closure);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(500, $response->status());
        $this->assertEquals(json_encode($data), $response->content());
    }

    public function testPresentStatusSuccess(): void
    {
        $response = $this->mockView('SUCCESS');

        $this->assertEquals(200, $response->status());
    }

    public function testPresentStatusForbidden(): void
    {
        $response = $this->mockView('FORBIDDEN_ERROR');

        $this->assertEquals(403, $response->status());
    }

    public function testPresentStatusNotFound(): void
    {
        $response = $this->mockView('NOT_FOUND_ERROR');

        $this->assertEquals(404, $response->status());
    }

    public function testPresentStatusValidationError(): void
    {
        $response = $this->mockView('VALIDATION_ERROR');

        $this->assertEquals(422, $response->status());
    }

    /**
     * @param string $result_status
     *
     * @return JsonResponse
     */
    protected function mockView(string $result_status): JsonResponse
    {
        $result = m::mock(DomainActionResultContract::class);
        $result->shouldReceive('status')
            ->once()
            ->andReturn($result_status);

        $closure = fn () => null;

        $baseView = m::mock(DomainActionResultHttpArray::class);
        $baseView->shouldReceive('present')
            ->once()
            ->with($result, $closure)
            ->andReturn([]);

        $view = new DomainActionResultHttpJsonResponse($baseView);

        return $view->present($result, $closure);
    }
}