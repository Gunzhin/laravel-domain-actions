# Laravel Domain Actions

Simple wrapper for organization your application actions.
Returning a unified result, logging messages and errors, convenient HTTP presentation, exception handling.
## Requirements

- **PHP 8.1+**
- **Laravel Framework 9.25+**

## Installation

First add repository to composer.json:

```json
...
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/Gunzhin/laravel-domain-actions.git"
    }
]
```

Later run command in console:

```bash
composer require gunzhin/laravel-domain-actions
```

## Usage

Create action class and use it in controller:

```php
// GetInputDumpAction.php

namespace App\Actions;

use DomainActions\DomainAction;

class GetInputDumpAction extends DomainAction
{
    /**
     * @var string
     */
    protected string $input;

    /**
     * @param array $parameters
     *
     * @return void
     */
    public function setParameters(array $parameters): void
    {
        $this->input = $parameters[0] ?? '';
    }

    /**
     * @return string[]
     */
    public function process(): array
    {
        return [
            'input' => $this->input
        ];
    }
}
```

```php
// Controller.php

use App\Actions\GetInputDumpAction;

public function action(Request $request, GetInputDumpAction $action): JsonResponse
{
    $input = $request->get('input');

    $result = $action->set($input)->run();

    /** @var DomainActionResultViewContract $view */
    $view = app(DomainActionResultHttpJsonResponse::class);

    return $view->present($result);
}
```

## License

Laravel Domain Actions is open-sourced software licensed under the [MIT license](LICENSE.md).
