<?php

declare(strict_types=1);

namespace DomainActions;

use DomainActions\Contracts\DomainActionResultContract;
use DomainActions\Contracts\DomainActionResultViewContract;
use Closure;

class DomainActionResultHttpArray implements DomainActionResultViewContract
{
    /**
     * @param DomainActionResultContract $result
     * @param Closure|null $closure
     *
     * @return array
     */
    public function present(DomainActionResultContract $result, ?Closure $closure = null): array
    {
        $messages = $result->messages()->all();

        $data = [
            'status' => $result->status(),
            'message' => array_pop($messages),
        ];

        if ($result->isSuccess()) {
            $data = array_merge(
                $data,
                $result->payload()->toArray()
            );
        } elseif ($result->status() === $result::STATUS_EXCEPTION_ERROR) {
            if (config('app.debug') === true) {
                $data['exception'] = (string) $result->exception();
            }
        }

        if ($closure !== null) {
            $data = $closure($data);
        }

        return $data;
    }
}
