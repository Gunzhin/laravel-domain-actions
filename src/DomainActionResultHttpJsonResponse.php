<?php

declare(strict_types=1);

namespace DomainActions;

use DomainActions\Contracts\DomainActionResultContract;
use DomainActions\Contracts\DomainActionResultViewContract;
use Closure;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DomainActionResultHttpJsonResponse implements DomainActionResultViewContract
{
    /**
     * @var DomainActionResultHttpArray
     */
    protected DomainActionResultHttpArray $baseView;

    /**
     * @param DomainActionResultHttpArray $baseView
     */
    public function __construct(DomainActionResultHttpArray $baseView)
    {
        $this->baseView = $baseView;
    }

    /**
     * @param DomainActionResultContract $result
     * @param Closure|null $closure
     *
     * @return JsonResponse
     */
    public function present(DomainActionResultContract $result, ?Closure $closure = null): JsonResponse
    {
        $data = $this->baseView->present($result, $closure);

        $status = match ($result->status()) {
            $result::STATUS_SUCCESS => Response::HTTP_OK,
            $result::STATUS_FORBIDDEN_ERROR => Response::HTTP_FORBIDDEN,
            $result::STATUS_NOT_FOUND_ERROR => Response::HTTP_NOT_FOUND,
            $result::STATUS_VALIDATION_ERROR => Response::HTTP_UNPROCESSABLE_ENTITY,
            default => Response::HTTP_INTERNAL_SERVER_ERROR,
        };

        return new JsonResponse($data, $status);
    }
}
