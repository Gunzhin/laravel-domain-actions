<?php

declare(strict_types=1);

namespace DomainActions;

use DomainActions\Contracts\DomainActionResultContract;
use DomainActions\Exceptions\InvalidResultParameters;
use Illuminate\Support\Collection;
use Throwable;

class DomainActionResult implements DomainActionResultContract
{
    /**
     * @var string
     */
    protected string $status;

    /**
     * @var Collection
     */
    protected Collection $messages;

    /**
     * @var Collection
     */
    protected Collection $payload;

    /**
     * @var Throwable|null
     */
    protected ?Throwable $exception;

    /**
     * OperationResult constructor.
     *
     * @param string $status
     * @param Collection $messages
     * @param Collection $payload
     * @param Throwable|null $exception
     *
     * @throws InvalidResultParameters
     */
    public function __construct(
        string $status,
        Collection $messages,
        Collection $payload,
        ?Throwable $exception = null
    ) {
        if (! in_array($status, $this->getAvailableStatuses())) {
            throw new InvalidResultParameters($status);
        }

        $this->status = $status;

        if ($messages->isEmpty()) {
            throw new InvalidResultParameters($status);
        }

        $this->messages = $messages;

        if (
            $status === self::STATUS_SUCCESS &&
            $exception !== null
        ) {
            throw new InvalidResultParameters(get_class($exception));
        } elseif (
            $status === self::STATUS_EXCEPTION_ERROR &&
            (
                $exception === null ||
                $payload->isNotEmpty()
            )
        ) {
            throw new InvalidResultParameters();
        }

        $this->payload = $payload;
        $this->exception = $exception;
    }

    /**
     * @return string
     */
    public function status(): string
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->status === self::STATUS_SUCCESS;
    }

    /**
     * @return Collection
     */
    public function messages(): Collection
    {
        return $this->messages;
    }

    /**
     * @return Collection
     */
    public function payload(): Collection
    {
        return $this->payload;
    }

    /**
     * @return Throwable|null
     */
    public function exception(): ?Throwable
    {
        return $this->exception;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'status' => $this->status,
            'messages' => $this->messages->toArray(),
            'payload' => $this->payload->toArray(),
            'exception' => $this->exception
                ? (string) $this->exception
                : null,
        ];
    }

    /**
     * @return array
     */
    protected function getAvailableStatuses(): array
    {
        return [
            self::STATUS_SUCCESS,
            self::STATUS_FAILED,
            self::STATUS_VALIDATION_ERROR,
            self::STATUS_FORBIDDEN_ERROR,
            self::STATUS_NOT_FOUND_ERROR,
            self::STATUS_EXCEPTION_ERROR,
        ];
    }
}
