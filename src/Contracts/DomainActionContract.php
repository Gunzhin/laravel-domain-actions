<?php

declare(strict_types=1);

namespace DomainActions\Contracts;

interface DomainActionContract
{
    /**
     * @param mixed ...$parameters
     *
     * @return $this
     */
    public function set(...$parameters): self;

    /**
     * @return DomainActionResultContract
     */
    public function run(): DomainActionResultContract;
}
