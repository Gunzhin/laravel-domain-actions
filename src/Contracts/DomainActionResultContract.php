<?php

declare(strict_types=1);

namespace DomainActions\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Throwable;

interface DomainActionResultContract extends Arrayable
{
    public const STATUS_SUCCESS = 'SUCCESS';
    public const STATUS_FAILED = 'FAILED';
    public const STATUS_VALIDATION_ERROR = 'VALIDATION_ERROR';
    public const STATUS_FORBIDDEN_ERROR = 'FORBIDDEN_ERROR';
    public const STATUS_NOT_FOUND_ERROR = 'NOT_FOUND_ERROR';
    public const STATUS_EXCEPTION_ERROR = 'EXCEPTION_ERROR';

    /**
     * @return string
     */
    public function status(): string;

    /**
     * @return Collection
     */
    public function messages(): Collection;

    /**
     * @return bool
     */
    public function isSuccess(): bool;

    /**
     * @return Collection
     */
    public function payload(): Collection;

    /**
     * @return Throwable|null
     */
    public function exception(): ?Throwable;
}
