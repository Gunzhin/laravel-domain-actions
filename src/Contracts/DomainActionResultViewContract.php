<?php

declare(strict_types=1);

namespace DomainActions\Contracts;

use Closure;

interface DomainActionResultViewContract
{
    /**
     * @param DomainActionResultContract $result
     * @param Closure|null $closure
     *
     * @return mixed
     */
    public function present(DomainActionResultContract $result, ?Closure $closure = null);
}
