<?php

declare(strict_types=1);

namespace DomainActions;

use DomainActions\Contracts\DomainActionContract;
use DomainActions\Contracts\DomainActionResultContract;
use DomainActions\Exceptions\InvalidActionParameters;
use DomainActions\Exceptions\InvalidResultParameters;
use Illuminate\Log\LogManager;
use Illuminate\Support\Collection;
use Psr\Log\LoggerInterface;
use Throwable;
use ReflectionClass;

abstract class DomainAction implements DomainActionContract
{
    /**
     * @var string|null
     */
    protected ?string $status;

    /**
     * @var Collection
     */
    protected Collection $messages;

    /**
     * @var LoggerInterface|null
     */
    protected ?LoggerInterface $logger;

    /**
     * @var bool
     */
    protected bool $has_logging = true;


    public function __construct()
    {
        $this->status = null;
        $this->messages = new Collection();
        $this->logger = $this->initLogger();
    }

    /**
     * @param ...$parameters
     *
     * @return DomainActionContract
     *
     * @throws
     */
    public function set(...$parameters): DomainActionContract
    {
        try {
            $this->setParameters($parameters);

            $this->addMessage('Set parameters: ' . json_encode($parameters, JSON_UNESCAPED_UNICODE));
        } catch (Throwable $exception) {
            throw new InvalidActionParameters($exception->getMessage());
        }

        return $this;
    }

    /**
     * @return DomainActionResultContract
     *
     * @throws InvalidResultParameters
     */
    public function run(): DomainActionResultContract
    {
        try {
            $payload = $this->process();

            if ($this->canProcessing()) {
                $this->setStatus(DomainActionResultContract::STATUS_SUCCESS);
            }
        } catch (Throwable $exception) {
            $this->addMessage(__('Internal service error.'));
            $this->addMessage($exception->getMessage(), true);
            $this->setStatus(DomainActionResultContract::STATUS_EXCEPTION_ERROR);
        }

        $this->addMessage('Status: ' . $this->status, true);

        return new DomainActionResult(
            $this->status,
            $this->messages,
            new Collection($payload ?? []),
            $exception ?? null
        );
    }

    /**
     * @param array $parameters
     *
     * @return void
     */
    abstract protected function setParameters(array $parameters): void;

    /**
     * @return array
     */
    abstract protected function process(): array;

    /**
     * @param string $message
     * @param bool $debug_only
     *
     * @return void
     */
    protected function addMessage(string $message, bool $debug_only = false): void
    {
        $can_show = (
            ! $debug_only ||
            config('debug') === true
        );

        if (
            $this->canProcessing() &&
            $can_show
        ) {
            $this->messages->push($message);
        }

        $message = sprintf('%s: %s',
            (new ReflectionClass($this))->getShortName(),
            $message
        );

        $this->logger?->info($message);
    }

    /**
     * @param string $status
     * @return void
     */
    protected function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    protected function canProcessing(): bool
    {
        return $this->status === null;
    }

    /**
     * @return LoggerInterface|null
     */
    protected function initLogger(): ?LoggerInterface
    {
        $logger = null;

        if ($this->has_logging) {
            /** @var LogManager $logManager */
            $logManager = app('log');

            $channel = ! empty(config('logging.channels.actions'))
                ? 'actions'
                : config('logging.default');

            if ($channel !== null) {
                $logger = $logManager->channel($channel);
            }
        }

        return $logger;
    }
}
